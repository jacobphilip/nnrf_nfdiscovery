/*
 * NRF NFDiscovery Service
 *
 * NRF NFDiscovery Service. © 2021, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC). All rights reserved. 
 *
 * API version: 1.1.3
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// LmfInfo Information of an LMF NF Instance
type LmfInfo struct {
	ServingClientTypes *[]ExternalClientType `json:"servingClientTypes,omitempty"`
	LmfId *string `json:"lmfId,omitempty"`
	ServingAccessTypes *[]AccessType `json:"servingAccessTypes,omitempty"`
	ServingAnNodeTypes *[]AnNodeType `json:"servingAnNodeTypes,omitempty"`
	ServingRatTypes *[]RatType `json:"servingRatTypes,omitempty"`
}

// NewLmfInfo instantiates a new LmfInfo object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewLmfInfo() *LmfInfo {
	this := LmfInfo{}
	return &this
}

// NewLmfInfoWithDefaults instantiates a new LmfInfo object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewLmfInfoWithDefaults() *LmfInfo {
	this := LmfInfo{}
	return &this
}

// GetServingClientTypes returns the ServingClientTypes field value if set, zero value otherwise.
func (o *LmfInfo) GetServingClientTypes() []ExternalClientType {
	if o == nil || o.ServingClientTypes == nil {
		var ret []ExternalClientType
		return ret
	}
	return *o.ServingClientTypes
}

// GetServingClientTypesOk returns a tuple with the ServingClientTypes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LmfInfo) GetServingClientTypesOk() (*[]ExternalClientType, bool) {
	if o == nil || o.ServingClientTypes == nil {
		return nil, false
	}
	return o.ServingClientTypes, true
}

// HasServingClientTypes returns a boolean if a field has been set.
func (o *LmfInfo) HasServingClientTypes() bool {
	if o != nil && o.ServingClientTypes != nil {
		return true
	}

	return false
}

// SetServingClientTypes gets a reference to the given []ExternalClientType and assigns it to the ServingClientTypes field.
func (o *LmfInfo) SetServingClientTypes(v []ExternalClientType) {
	o.ServingClientTypes = &v
}

// GetLmfId returns the LmfId field value if set, zero value otherwise.
func (o *LmfInfo) GetLmfId() string {
	if o == nil || o.LmfId == nil {
		var ret string
		return ret
	}
	return *o.LmfId
}

// GetLmfIdOk returns a tuple with the LmfId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LmfInfo) GetLmfIdOk() (*string, bool) {
	if o == nil || o.LmfId == nil {
		return nil, false
	}
	return o.LmfId, true
}

// HasLmfId returns a boolean if a field has been set.
func (o *LmfInfo) HasLmfId() bool {
	if o != nil && o.LmfId != nil {
		return true
	}

	return false
}

// SetLmfId gets a reference to the given string and assigns it to the LmfId field.
func (o *LmfInfo) SetLmfId(v string) {
	o.LmfId = &v
}

// GetServingAccessTypes returns the ServingAccessTypes field value if set, zero value otherwise.
func (o *LmfInfo) GetServingAccessTypes() []AccessType {
	if o == nil || o.ServingAccessTypes == nil {
		var ret []AccessType
		return ret
	}
	return *o.ServingAccessTypes
}

// GetServingAccessTypesOk returns a tuple with the ServingAccessTypes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LmfInfo) GetServingAccessTypesOk() (*[]AccessType, bool) {
	if o == nil || o.ServingAccessTypes == nil {
		return nil, false
	}
	return o.ServingAccessTypes, true
}

// HasServingAccessTypes returns a boolean if a field has been set.
func (o *LmfInfo) HasServingAccessTypes() bool {
	if o != nil && o.ServingAccessTypes != nil {
		return true
	}

	return false
}

// SetServingAccessTypes gets a reference to the given []AccessType and assigns it to the ServingAccessTypes field.
func (o *LmfInfo) SetServingAccessTypes(v []AccessType) {
	o.ServingAccessTypes = &v
}

// GetServingAnNodeTypes returns the ServingAnNodeTypes field value if set, zero value otherwise.
func (o *LmfInfo) GetServingAnNodeTypes() []AnNodeType {
	if o == nil || o.ServingAnNodeTypes == nil {
		var ret []AnNodeType
		return ret
	}
	return *o.ServingAnNodeTypes
}

// GetServingAnNodeTypesOk returns a tuple with the ServingAnNodeTypes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LmfInfo) GetServingAnNodeTypesOk() (*[]AnNodeType, bool) {
	if o == nil || o.ServingAnNodeTypes == nil {
		return nil, false
	}
	return o.ServingAnNodeTypes, true
}

// HasServingAnNodeTypes returns a boolean if a field has been set.
func (o *LmfInfo) HasServingAnNodeTypes() bool {
	if o != nil && o.ServingAnNodeTypes != nil {
		return true
	}

	return false
}

// SetServingAnNodeTypes gets a reference to the given []AnNodeType and assigns it to the ServingAnNodeTypes field.
func (o *LmfInfo) SetServingAnNodeTypes(v []AnNodeType) {
	o.ServingAnNodeTypes = &v
}

// GetServingRatTypes returns the ServingRatTypes field value if set, zero value otherwise.
func (o *LmfInfo) GetServingRatTypes() []RatType {
	if o == nil || o.ServingRatTypes == nil {
		var ret []RatType
		return ret
	}
	return *o.ServingRatTypes
}

// GetServingRatTypesOk returns a tuple with the ServingRatTypes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LmfInfo) GetServingRatTypesOk() (*[]RatType, bool) {
	if o == nil || o.ServingRatTypes == nil {
		return nil, false
	}
	return o.ServingRatTypes, true
}

// HasServingRatTypes returns a boolean if a field has been set.
func (o *LmfInfo) HasServingRatTypes() bool {
	if o != nil && o.ServingRatTypes != nil {
		return true
	}

	return false
}

// SetServingRatTypes gets a reference to the given []RatType and assigns it to the ServingRatTypes field.
func (o *LmfInfo) SetServingRatTypes(v []RatType) {
	o.ServingRatTypes = &v
}

func (o LmfInfo) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.ServingClientTypes != nil {
		toSerialize["servingClientTypes"] = o.ServingClientTypes
	}
	if o.LmfId != nil {
		toSerialize["lmfId"] = o.LmfId
	}
	if o.ServingAccessTypes != nil {
		toSerialize["servingAccessTypes"] = o.ServingAccessTypes
	}
	if o.ServingAnNodeTypes != nil {
		toSerialize["servingAnNodeTypes"] = o.ServingAnNodeTypes
	}
	if o.ServingRatTypes != nil {
		toSerialize["servingRatTypes"] = o.ServingRatTypes
	}
	return json.Marshal(toSerialize)
}

type NullableLmfInfo struct {
	value *LmfInfo
	isSet bool
}

func (v NullableLmfInfo) Get() *LmfInfo {
	return v.value
}

func (v *NullableLmfInfo) Set(val *LmfInfo) {
	v.value = val
	v.isSet = true
}

func (v NullableLmfInfo) IsSet() bool {
	return v.isSet
}

func (v *NullableLmfInfo) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableLmfInfo(val *LmfInfo) *NullableLmfInfo {
	return &NullableLmfInfo{value: val, isSet: true}
}

func (v NullableLmfInfo) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableLmfInfo) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


