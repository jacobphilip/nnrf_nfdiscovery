# AfEventAnyOf

## Enum


* `SVC_EXPERIENCE` (value: `"SVC_EXPERIENCE"`)

* `UE_MOBILITY` (value: `"UE_MOBILITY"`)

* `UE_COMM` (value: `"UE_COMM"`)

* `EXCEPTIONS` (value: `"EXCEPTIONS"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


