# LmfInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ServingClientTypes** | Pointer to [**[]ExternalClientType**](ExternalClientType.md) |  | [optional] 
**LmfId** | Pointer to **string** |  | [optional] 
**ServingAccessTypes** | Pointer to [**[]AccessType**](AccessType.md) |  | [optional] 
**ServingAnNodeTypes** | Pointer to [**[]AnNodeType**](AnNodeType.md) |  | [optional] 
**ServingRatTypes** | Pointer to [**[]RatType**](RatType.md) |  | [optional] 

## Methods

### NewLmfInfo

`func NewLmfInfo() *LmfInfo`

NewLmfInfo instantiates a new LmfInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLmfInfoWithDefaults

`func NewLmfInfoWithDefaults() *LmfInfo`

NewLmfInfoWithDefaults instantiates a new LmfInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetServingClientTypes

`func (o *LmfInfo) GetServingClientTypes() []ExternalClientType`

GetServingClientTypes returns the ServingClientTypes field if non-nil, zero value otherwise.

### GetServingClientTypesOk

`func (o *LmfInfo) GetServingClientTypesOk() (*[]ExternalClientType, bool)`

GetServingClientTypesOk returns a tuple with the ServingClientTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetServingClientTypes

`func (o *LmfInfo) SetServingClientTypes(v []ExternalClientType)`

SetServingClientTypes sets ServingClientTypes field to given value.

### HasServingClientTypes

`func (o *LmfInfo) HasServingClientTypes() bool`

HasServingClientTypes returns a boolean if a field has been set.

### GetLmfId

`func (o *LmfInfo) GetLmfId() string`

GetLmfId returns the LmfId field if non-nil, zero value otherwise.

### GetLmfIdOk

`func (o *LmfInfo) GetLmfIdOk() (*string, bool)`

GetLmfIdOk returns a tuple with the LmfId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLmfId

`func (o *LmfInfo) SetLmfId(v string)`

SetLmfId sets LmfId field to given value.

### HasLmfId

`func (o *LmfInfo) HasLmfId() bool`

HasLmfId returns a boolean if a field has been set.

### GetServingAccessTypes

`func (o *LmfInfo) GetServingAccessTypes() []AccessType`

GetServingAccessTypes returns the ServingAccessTypes field if non-nil, zero value otherwise.

### GetServingAccessTypesOk

`func (o *LmfInfo) GetServingAccessTypesOk() (*[]AccessType, bool)`

GetServingAccessTypesOk returns a tuple with the ServingAccessTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetServingAccessTypes

`func (o *LmfInfo) SetServingAccessTypes(v []AccessType)`

SetServingAccessTypes sets ServingAccessTypes field to given value.

### HasServingAccessTypes

`func (o *LmfInfo) HasServingAccessTypes() bool`

HasServingAccessTypes returns a boolean if a field has been set.

### GetServingAnNodeTypes

`func (o *LmfInfo) GetServingAnNodeTypes() []AnNodeType`

GetServingAnNodeTypes returns the ServingAnNodeTypes field if non-nil, zero value otherwise.

### GetServingAnNodeTypesOk

`func (o *LmfInfo) GetServingAnNodeTypesOk() (*[]AnNodeType, bool)`

GetServingAnNodeTypesOk returns a tuple with the ServingAnNodeTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetServingAnNodeTypes

`func (o *LmfInfo) SetServingAnNodeTypes(v []AnNodeType)`

SetServingAnNodeTypes sets ServingAnNodeTypes field to given value.

### HasServingAnNodeTypes

`func (o *LmfInfo) HasServingAnNodeTypes() bool`

HasServingAnNodeTypes returns a boolean if a field has been set.

### GetServingRatTypes

`func (o *LmfInfo) GetServingRatTypes() []RatType`

GetServingRatTypes returns the ServingRatTypes field if non-nil, zero value otherwise.

### GetServingRatTypesOk

`func (o *LmfInfo) GetServingRatTypesOk() (*[]RatType, bool)`

GetServingRatTypesOk returns a tuple with the ServingRatTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetServingRatTypes

`func (o *LmfInfo) SetServingRatTypes(v []RatType)`

SetServingRatTypes sets ServingRatTypes field to given value.

### HasServingRatTypes

`func (o *LmfInfo) HasServingRatTypes() bool`

HasServingRatTypes returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


